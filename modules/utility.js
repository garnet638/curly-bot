const Discord = require('discord.js');
const curlyB = require('../curly.js');
const bot = curlyB.ebot;
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js")
const prefix = botInfo.prefix;
const botToken = botInfo.token;

function msToTime(s) {
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;

  return hrs + ':' + mins + ':' + secs + '.' + ms;
}

module.exports = {
  name : "🛠 Utility",

  version : 1,

  help : {
    "bot" : "`Usage: " + prefix + botInfo.botCommand + " [CATEGORY]`\nInformation about " + botInfo.botName + " \nSubcommands: `who` `stats`",
    "userinfo" : "`Usage: " + prefix + "userinfo [PERSON]`\nInformation about a user",
    "serverinfo" : "`Usage: " + prefix + "serverinfo`\nInformation about the server you're currently on",
    "install" : "`Usage: " + prefix + "install`\nInstall Curly",
    "info" : "`Usage: " + prefix + "info`\nInstall Curly"
  },

  main :  function (message, user, command, args){

    var gUser = message.guild.member(user);

    if (command == botInfo.botCommand || command == "bot") {
      //Who is curly

      if (args[0] == "who"){
        whoAmI = "";
        whoAmI += "Hi " + user + ", my name is " + botInfo.botName + "!\n";
        whoAmI += botInfo.who;
        message.channel.sendFile("/../avatar.png", "me.png", whoAmI);
      }
      if (args[0] == "stats"){
        stats = "\nStats about " + botInfo.botName;
        stats += "\nServers: `" + bot.guilds.keyArray().length + "`";
        stats += "\nChannels: `" + bot.channels.keyArray().length + "`";
        stats += "\nUsers: `" + bot.users.keyArray().length + "`";
        stats += "\nUptime: `" + msToTime(bot.uptime) + "`";
        message.channel.sendMessage(user + stats);
      }
      else{
        message.channel.sendMessage(user + "\nInformation about " + botInfo.botName + " \nSubcommands: `who` `stats`");
      }
    }

    //Userinfo
    else if (command == "userinfo") {
      if (args.length == 0){
        var person = user;
        var guildPerson = message.guild.member(person);
      }
      else{
        var person = message.mentions.users.first();
        var guildPerson = message.guild.member(person);
      }

      var roles = guildPerson.roles.array()

      for (var i = 0; i < roles.length; i++){
        roles[i] = roles[i].name;
      }

      userinfo = "```";
      userinfo += "\nInformation on: " + person.username;
      userinfo += "\nAre they a bot? " + person.bot;
      userinfo += "\nCreated on: " + person.createdAt;
      userinfo += "\nJoined on: " + guildPerson.joinedAt;
      userinfo += "\nNickname: " + guildPerson.nickname;
      userinfo += "\nRoles: " + roles.join(", ");
      if (person.avatarURL == null){
        userinfo += "\nAvatar: No avatar set```";
        message.channel.sendMessage(userinfo);
      }
      else{
        userinfo += "\nAvatar: ```";
        message.channel.sendFile(person.avatarURL, "avatar.png", userinfo);
      }
    }

    //Serverinfo
    else if (command == "serverinfo") {
      var server = message.guild;
      var roles = server.roles.array()

      for (var i = 0; i < roles.length; i++){
        roles[i] = roles[i].name;
      }

      var serverinfo = "```";
      serverinfo += "\nInformation on: " + server.name;
      serverinfo += "\nOwner: " + server.owner.user.username;
      serverinfo += "\nCreated on: " + server.createdAt;
      serverinfo += "\nChannels: " + server.channels.array().length;
      serverinfo += "\nDefault Channel: " + server.defaultChannel.name;
      serverinfo += "\nMembers: " + server.memberCount;
      serverinfo += "\nRoles: " + roles.length + " roles || " + roles;
      if (server.iconURL == null){
        serverinfo += "\nIcon: No icon set```";
        message.channel.sendMessage(serverinfo);
      }
      else{
        serverinfo += "\nIcon: ```";
        message.channel.sendFile(server.iconURL, "icon.png", serverinfo);
      }

    }

    //Install
    else if (command == "install" || command == "info") {
      message.channel.sendMessage(user + "\n**`CurlyBot`** is a multi-function highly customizable bot written in Discord.JS\n**GitHub:** https://gitlab.com/garnet638/curly-bot\n**Bitcoin:** 1BXkcKzoBrPvKK3tqbWs43w7zggeGTVaMz");
    }

  //End, don't write past here
  }
};
