const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
var econ = require("./data/economy.json");
const prefix = botInfo.prefix;

var slotNums = ["🍒", "🍀", "🍋", "7⃣"];
var slotPayout = ["10", "50", "75", "100"];
var cards = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
var values = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
var ddeck = ['A♥', '2♥', '3♥', '4♥', '5♥', '6♥', '7♥', '8♥', '9♥', '10♥', 'J♥', 'Q♥', 'K♥', 'A♦', '2♦', '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '10♦', 'J♦', 'Q♦', 'K♦', 'A♣', '2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '10♣', 'J♣', 'Q♣', 'K♣', 'A♠', '2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '10♠', 'J♠', 'Q♠', 'K♠'];
var shuffled = [];
var bj = {};

module.exports = {

  name : "🍒 Casino",

  version : 1,

  //The module's help files
  help : {
    "deck" : "`Usage: " + prefix + "deck [CARDS]`\nGive a randomized deck. Leave blank for the full 52 cards",
    "slot" : "`Usage: " + prefix + "slot <AMOUNT>`\nTry your hand in a slot machine",
    "blackjack" : "`Usage: " + prefix + "blackjack`\nAlias `21`, `bj`\nStart a game of blackjack",
    "highroller" : "`Usage: " + prefix + "highroller <AMOUNT>`\nRisk it all in a high-stakes take it or lose it game",
  },

  //The module's commands
  main :  function (message, user, command, args){

    if (utils.checkIfLoaded("economy")){

      server = message.guild;
      bet = Math.abs(utils.makeInt(0, args, message, 0, "Please enter a valid number for your bet. It has been defaulted to 0"));
      if (bet > utils.getMoney(server, user)){
        bet = utils.getMoney(server, user);
      }

      if (command == "deck"){
        if (args[0] == null){
          args[0] = 52;
        }
        num = utils.makeInt(0, args, message, 52);
        shuffled = utils.shuffle(ddeck);
        message.channel.sendMessage(user + " `" + shuffled.slice(0, num).join("`, `") + "`");
      }

      else if (command == "slot"){
        if (bet <= utils.getMoney(server, user)){
          var msg = user + "\n️🎰️🎰️🎰️\n";
          var rnd = 0;
          var row = {
            "1": [],
            "2": [],
            "3": []
          };
          for (i = 0; i<3; i++){
            rnd = Math.floor(Math.random()*slotNums.length);
            row["1"].push(slotNums[rnd]);
            rnd++;
            if (rnd == slotNums.length){rnd = 0;}
            row["2"].push(slotNums[rnd]);
            rnd++;
            if (rnd == slotNums.length){rnd = 0;}
            row["3"].push(slotNums[rnd]);
          }
          for (i = 1; i<4; i++){
            for (j=0; j<3; j++){
              msg += row[i.toString()][j];
            }
            msg += "\n";
          }
          msg += "🎰️🎰️🎰️\n";
          if (row["2"][0] == row["2"][1] && row["2"][1] == row["2"][2]){
            msg += "**WINNER!** You won: $" + slotPayout[slotNums.indexOf(row["2"][0])]*bet;
            utils.addMoney(server, user, slotPayout[slotNums.indexOf(row["2"][0])]*bet);
          }
          else{
            msg += "You just lost $"+ bet;
            utils.addMoney(server, user, bet*-1);
          }
          message.channel.sendMessage(msg);
        }
        else{
          message.channel.sendMessage(user + ", you do not have enough money to make that bet.")
        }
      }

      else if (command == "blackjack" || command == "21" || command == "bj") {
        if (args.length == 0){
          message.channel.sendMessage("**Subcommands:** `hit` `fold` `bet`\nUse **" + prefix + "blackjack bet [NUMBER]** to start a new game");
        }
        //Game
        if (args.length == 1){
          if (!bj.hasOwnProperty(server.id) || !bj[server.id].hasOwnProperty(user.id) || bj[server.id][user.id]["playing"] == false){
            message.channel.sendMessage("Please use **" + prefix + "blackjack bet [NUMBER]** to start a new game");
          }
          else{
            if (args[0] == "hit"){
              bj[server.id][user.id]["player"] += bj[server.id][user.id]["deck"][0] + " ";
              bj[server.id][user.id]["playertotal"] += values[ddeck.indexOf(bj[server.id][user.id]["deck"][0].toString())];
              bj[server.id][user.id]["deck"].shift();
              if (bj[server.id][user.id]["housetotal"] <= 17){
                bj[server.id][user.id]["house"] += bj[server.id][user.id]["deck"][0] + " ";
                bj[server.id][user.id]["housetotal"] += values[ddeck.indexOf(bj[server.id][user.id]["deck"][0].toString())];
                bj[server.id][user.id]["deck"].shift();
              }
              if (bj[server.id][user.id]["playertotal"] >= 21 || bj[server.id][user.id]["housetotal"] >= 21){
                bj[server.id][user.id]["playing"] = false;
                if (bj[server.id][user.id]["playertotal"] == 21 || bj[server.id][user.id]["housetotal"] > 21){
                  message.channel.sendMessage("**YOU WON!** Gains: $" + (bj[server.id][user.id]["bet"] * 2) + "\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: (" + bj[server.id][user.id]["housetotal"] + ") " + bj[server.id][user.id]["housef"] + bj[server.id][user.id]["house"]);
                  utils.addMoney(server, user, (bj[server.id][user.id]["bet"]*2));
                }
                else if (bj[server.id][user.id]["playertotal"] > 21 || bj[server.id][user.id]["housetotal"] == 21){
                  message.channel.sendMessage("**THE DEALER WON!** Losses: $" + bj[server.id][user.id]["bet"] + "\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: (" + bj[server.id][user.id]["housetotal"] + ") " + bj[server.id][user.id]["housef"] + bj[server.id][user.id]["house"]);
                  utils.addMoney(server, user, (bj[server.id][user.id]["bet"]*-1));
                }
              }
              else{
                message.channel.sendMessage("**BLACKJACK**\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: ?? " + bj[server.id][user.id]["house"]);
              }
            }
            if (args[0] == "fold"){
              bj[server.id][user.id]["playing"] = false;
              while (bj[server.id][user.id]["housetotal"] <= 17){
                bj[server.id][user.id]["house"] += bj[server.id][user.id]["deck"][0] + " ";
                bj[server.id][user.id]["housetotal"] += values[ddeck.indexOf(bj[server.id][user.id]["deck"][0].toString())];
                bj[server.id][user.id]["deck"].shift();
              }
              if ((bj[server.id][user.id]["playertotal"] > bj[server.id][user.id]["housetotal"] && bj[server.id][user.id]["playertotal"] <= 21) || bj[server.id][user.id]["housetotal"] > 21){
                message.channel.sendMessage("**YOU WON!** Gains: $" + (bj[server.id][user.id]["bet"] * 2) + "\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: (" + bj[server.id][user.id]["housetotal"] + ") " + bj[server.id][user.id]["housef"] + bj[server.id][user.id]["house"]);
                utils.addMoney(server, user, (bj[server.id][user.id]["bet"]*2));
              }
              else if ((bj[server.id][user.id]["housetotal"] > bj[server.id][user.id]["playertotal"] && bj[server.id][user.id]["housetotal"] <= 21) || bj[server.id][user.id]["playertotal"] > 21){
                message.channel.sendMessage("**THE DEALER WON!** Losses: $" + bj[server.id][user.id]["bet"] + "\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: (" + bj[server.id][user.id]["housetotal"] + ") " + bj[server.id][user.id]["housef"] + bj[server.id][user.id]["house"]);
                utils.addMoney(server, user, (bj[server.id][user.id]["bet"]*-1));
              }
              else{
                message.channel.sendMessage("**DEALER PUSH!** You get your money back\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: (" + bj[server.id][user.id]["housetotal"] + ") " + bj[server.id][user.id]["housef"] + bj[server.id][user.id]["house"]);
              }
            }
          }
        }
        //Start a new game
        if (args.length == 2 && args[0] == "bet"){
          if (!bj.hasOwnProperty(server.id)){
            bj[server.id] = {};
          }
          bj[server.id][user.id] = {
            "playing" : true,
            "housef" : "",
            "house" : "",
            "housetotal" : 0,
            "player" : "",
            "playertotal" : 0,
            "bet" : 0,
            "deck" : []
          };
          bj[server.id][user.id]["deck"] = utils.shuffle(utils.shuffle(ddeck));
          bj[server.id][user.id]["bet"] = Math.abs(utils.makeInt(1, args, message, 0, "Please enter a valid number for your bet. It has been defaulted to 0"));
          console.log(Math.abs(utils.makeInt(1, args, message, 0, "Please enter a valid number for your bet. It has been defaulted to 0")))

          for (i = 0; i < 2; i++){
            if (i == 0) {bj[server.id][user.id]["housef"] += bj[server.id][user.id]["deck"][0] + " ";}
            if (i == 1) {bj[server.id][user.id]["house"] += bj[server.id][user.id]["deck"][0] + " ";}
            bj[server.id][user.id]["housetotal"] += values[ddeck.indexOf(bj[server.id][user.id]["deck"][0].toString())];
            bj[server.id][user.id].deck.shift();
            bj[server.id][user.id]["player"] += bj[server.id][user.id]["deck"][0] + " ";
            bj[server.id][user.id]["playertotal"] += values[ddeck.indexOf(bj[server.id][user.id]["deck"][0].toString())];
            bj[server.id][user.id]["deck"].shift();
          }
          console.log(bj[server.id][user.id]["bet"]*2);
          message.channel.sendMessage("**BLACKJACK**\nYou: (" + bj[server.id][user.id]["playertotal"] + ") " + bj[server.id][user.id]["player"] + "\nDealer: ?? " + bj[server.id][user.id]["house"]);
        }

      }

      else if (command == "highroller"){
        if (bet >= 10000){
          if (Math.floor(Math.random()*10)+1 < 3){
            message.channel.sendMessage(user + "  **Congratulations!** You won $" + bet*50);
            utils.addMoney(server, user, bet*50);
          }
          else{
            message.channel.sendMessage(user + "  **You lose...** Goodbye $" + bet);
            utils.addMoney(server, user, bet*-1);
          }
        }
        else{
          message.channel.sendMessage(user + ", you must have a bet of atleast $10000 to play");
        }
      }
    }
    else{
      message.channel.sendMessage("Sorry, that command requires the `economy` addon to be present")
    }
  //End, don't write past here
  }
};
