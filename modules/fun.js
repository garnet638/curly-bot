const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var stream = require("stream");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var swanson = require("./data/swanson.json");
var eightBall = require("./data/eightball.json");
const prefix = botInfo.prefix;

var jokes = [
  "What's worse than 🇨🇦 posters? 🇳🇫 posters (This is a /pol/ joke kiddos)",
  "Why did the chicken cross the road? To get to the other side.",
  "Doctor: \"I'm sorry but you suffer from a terminal illness and have only 10 to live.\"\nPatient: \"What do you mean, 10? 10 what? Months? Weeks?!\"\nDoctor: \"Nine.\" ",
  "My dog used to chase people on a bike a lot. It got so bad, finally I had to take his bike away.",
  "Jim, do you think I'm a bad mother?\nMy name is Paul.",
  "Scientists have now discovered how women keep their secrets. They do so within groups of 40.",
  "My life.",
  "My wife’s cooking is so bad we usually pray after our food.",
  "How can you tell you have a really bad case of acne?\nIt’s when the blind try to read your face."
];

module.exports = {

  name : "🎈 Fun",

  version : 1,

  help : {
    "ping" : "`Usage: " + prefix + "ping`\nPong!",
    "coinflip" : "`Usage: " + prefix + "coinflip`\nFlip a coin",
    "joke" : "`Usage: " + prefix + "joke`\nTell a funny joke",
    "8ball" : "`Usage: " + prefix + "8ball <QUESTION>`\nShake the magic 8 ball and get an answer",
    "chucknorris" : "`Usage: " + prefix + "chucknorris`\n2006",
    "swanson" : "`Usage: " + prefix + "swanson`\nGet a quote from Ron Fucking Swanson",
    "waifu" : "`Usage: " + prefix + "waifu <WAIFU>`\nRate your waifu",
    "roll" : "`Usage: " + prefix + "roll [MAX NUMBER] [ROLLS]`\nIn case you loose your 20-sided die",
    "bert" : "`Usage: " + prefix + "bert`\nbert"
  },

  main :  function (message, user, command, args){

    var joinedText =  message.content.split(" ").slice(1).join(" ");

    //Ping
    if (command == "ping") {
      message.channel.sendMessage("Pong!");
    }

    //Coin Flip
    else if (command == "coinflip") {
      var hts = Math.floor(Math.random()*2);
      if (hts == 1){
        message.channel.sendFile("http://i.imgur.com/iDj0vds.png", "heads.png", user + " **HEADS!**");
      }
      if (hts == 0){
        message.channel.sendFile("http://i.imgur.com/lakcV7g.png", "tails.png", user + " **TAILS!**");
      }
    }

    //8ball
    else if (command == "8ball") {
      if (args.length == 0){
        message.channel.sendMessage(user + ", please ask the magic 8 ball a question.");
      }
      else {
        message.channel.sendMessage(user + "\n🎱 " + eightBall[Math.floor(Math.random()*eightBall.length)]);
      }
    }

    else if (command == "joke") {
      message.channel.sendMessage(user + ", " + jokes[Math.floor(Math.random()*jokes.length)]);
    }

    else if (command == "chucknorris") {
      var pog = "";
      request('https://api.chucknorris.io/jokes/random', function (error, response, body) {
        if (!error && response.statusCode == 200) {
          pog = JSON.parse(body)
          message.channel.sendMessage(user + " Chuck Norris Fact #" + Math.floor(Math.random()*9999) + ": `" + pog.value + "`");
        }
      })
    }

    else if (command == "swanson") {
      message.channel.sendMessage(user + ", *\"" + swanson[Math.floor(Math.random()*swanson.length)] + "\"* -Ron Fucking Swanson");
    }

    else if (command == "waifu") {
      if (args.length == 0){
        message.channel.sendMessage(user + ", please enter a waifu to rate");
      }
      else if (joinedText.toLowerCase() == "lucina"){
        message.channel.sendMessage(user + ", Lucina is rated 10/10");
      }
      else {
        message.channel.sendMessage(user + ", " + joinedText + " is rated 🗑/10");
      }
    }

    else if (command == "say") {
      message.channel.sendMessage("How about you go screw yourself " + user);
    }

    else if (command == "roll") {
      if (args.length == 0){
        message.channel.sendMessage(user + " rolled " + (Math.floor(Math.random()*6) + 1));
      }
      if (args.length == 1){
        try {
          if (parseInt(args[0]) > 100000){
            message.channel.sendMessage(user + ", sorry, that number is too high (" + args[0] + " > 100000)");
          }
          else {
            message.channel.sendMessage(user + " rolled " + (Math.floor(Math.random()*parseInt(args[0])) + 1));
          }
        }
        catch (err) {
          message.channel.sendMessage(user + ", please input a number.");
        }
      }
      if (args.length >= 2){
        try {
          if (parseInt(args[0]) > 10000){
            message.channel.sendMessage(user + ", sorry, that number is too high (" + args[0] + " > 10000)");
          }
          else if (parseInt(args[1]) > 100){
            message.channel.sendMessage(user + ", sorry, that number is too high (" + args[1] + " > 100)");
          }
          else {
            var rolls = "";
            for (var i = 1; i <= parseInt(args[1]); i++){
              rolls += "`" + (Math.floor(Math.random()*parseInt(args[0])) + 1) + "` ";
            }
            message.channel.sendMessage(user + " rolled " + rolls);
          }
        }
        catch (err) {
          message.channel.sendMessage(user + ", please input a number.");
        }
      }
    }

    else if (command == "bert") {
      message.channel.sendFile("./modules/data/ernie.png", "bert.png", "");
    }

  //End, don't write past here
  }
};
