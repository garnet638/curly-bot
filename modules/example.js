const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
const prefix = botInfo.prefix;

///
/// NOTICE!
///
// This file is for example use only.
// Please, do not try to load it in.
// While it will load successfuly,
// it is meant for a base for other
// modules one might make. This file
// is literally just for copy/pasting.

module.exports = {

  //The module's name. Emojis optional, but gathered from http://emojipedia.org/
  name : "😀 Example",

  //The module's version
  version : 1,

  //The module's help files
  help : {
    "foo" : "`Usage: " + prefix + "foo`\nFoo",
    "bar" : "`Usage: " + prefix + "bar`\nBar"
  },

  //The module's commands
  main :  function (message, user, command, args){
    if (command == "foo"){
      message.channel.sendMessage("Foo");
    }

    else if (command == "bar") {
      message.channel.sendMessage("Bar");
    }
  //End, don't write past here
  }
};
