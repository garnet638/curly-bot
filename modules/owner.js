const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
//var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
const prefix = botInfo.prefix;

module.exports = {

  name : "🖥 Owner",

  version : 1,

  //The module's help files
  help : {
    "load" : "`Usage: " + prefix + "load <MODULE>`\nLoad a module",
    "unload" : "`Usage: " + prefix + "unload <MODULE>`\nUnload a module",
    "reload" : "`Usage: " + prefix + "reload <MODULE>`\nReload a module",
    "reload" : "`Usage: " + prefix + "name <NEW NAME>`\nRename your bot",
    "stop" : "`Usage: " + prefix + "stop`\nStop the bot"
  },

  //The module's commands
  main :  function (message, user, command, args){

    if (command == "kill"){
      if (utils.isOwner(user)){
        console.log(user.username + " successfuly stopped my process");
        message.channel.sendMessage("See you space cowboy");
        setTimeout(process.exit(), 500);
      }
      else{
        message.channel.sendMessage("How about no");
      }
    }

  //End, don't write past here
  }
};
