const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var warns = require("./data/warns.json");
var utils = require("../curlyUtils.js");
var svr = require("../server.json");
const prefix = botInfo.prefix;

module.exports = {

  name : "🛑 Moderation",

  version : 2,

  help : {
    "warn" : "`Usage: " + prefix + "warn <PERSON> <REASON>`\nWarn a user",
    "listwarns" : "`Usage: " + prefix + "listwarns`\nList a user's recorded warnings",
    "removewarn" : "`Usage: `" + prefix + "removewarn <PERSON>\nRemoves a warning from a user",
    "permlvl" : "`Usage: " + prefix + "permlvl [PERSON]`\nCheck a user's permission level",
    "clear" : "`Usage: " + prefix + "clear <MESSAGES> [PINS]`\nClear the specified amount of messages.\n[PINS] should be either *true* or *false*"
  },

  main :  function (message, user, command, args){

    var server = message.guild;
    //Attempt to get a user's mention
    try {
      var person = message.mentions.users.first();
      person = message.guild.member(person);
      var personID = person.id;
    }
    catch (err) {}


    //Warn a user
    if (command == "warn") {
        if (args.length < 2){
          message.channel.sendMessage(user + ", please specify a person to warn and a reason");
        }
        else if (message.mentions.users.first() == null) {
          message.channel.sendMessage(user + ", please specify a person to warn");
        }
        else{
          //This is a bit confusing to look at. TL;DR, if the person is able to warn, and the person has higher authority than the offender, the script fires
          if (utils.checkPerms(server, user) > 0 && utils.checkPerms(server, message.mentions.users.first()) < utils.checkPerms(server, user)){
            if (!warns.hasOwnProperty(server.id)) {
              warns[server.id] = {
                "maxwarns" : 3
              };
            }

            if (!warns[server.id].hasOwnProperty(personID)) {
              warns[server.id][personID] = {
                "warns" : {
                  "0" : {

                  }
                },
                "warnings" : 0,
                "totalwarnings" : 0
              };
            }

            if (warns[server.id][personID].warnings < warns[server.id].maxwarns){
              var reason = message.content.split(" ").slice(2).join(" ");
              warns[server.id][personID].warnings++;
              warns[server.id][personID].totalwarnings++;
              totalwarnings = warns[server.id][personID].totalwarnings;
              warns[server.id][personID]["warns"][warns[server.id][personID].totalwarnings] = user.username + ": " + reason;
              message.channel.sendMessage("✅ Warned " + person + " for `" + reason + "` Warnings: `" + warns[server.id][personID].warnings + "/" + warns[server.id].maxwarns + "`");
            };

            if (warns[server.id][personID].warnings == warns[server.id].maxwarns){
              var reason = message.content.split(" ").slice(2).join(" ");
              warns[server.id][personID].warnings = 0;
              person.ban(0);
              message.channel.sendMessage("✅ Banned " + person + ".");
            };

            var warnsJson = JSON.stringify(warns, null, 2);
            fs.writeFile(__dirname + '/data/warns.json', warnsJson, 'utf8');
          }
          else {
            message.channel.sendMessage(user + ", you do not have enough permissions to warn this user.\n(Use `{settings roles` if you do not have moderation roles set up)")
          }
        }
        }

    //List warnings
    else if (command == "listwarns" || command == "listwarn") {
      if (message.mentions.users.first() == null) {
        message.channel.sendMessage("Please enter a person's warnings to view");
      }
      else {
        if (!warns.hasOwnProperty([server.id])){
          warns[server.id] = {
            "maxwarns" : 3
          };
          message.channel.sendMessage(person + " has no warnings!");
          var warnsJson = JSON.stringify(warns, null, 2);
          fs.writeFile(__dirname + '/data/warns.json', warnsJson, 'utf8');
        }
        else if (!warns[server.id].hasOwnProperty(personID)){
          message.channel.sendMessage(person + " has no warnings!");
        }
        else{
          list = "Active warnings: `" + warns[server.id][personID].warnings + "/" + warns[server.id].maxwarns + "`\n";
          list += person + "'s warnings:\n";
          for (var i = 1; i <= warns[server.id][personID].totalwarnings; i++){
            list += "`" + warns[server.id][personID]["warns"][i] + "`\n";
          }
          message.channel.sendMessage(list);
        }
      }
    }

    //Remove a warn
    else if (command == "removewarn" || command == "removewarns") {
      if (utils.checkPerms(server, user) > 0 && utils.checkPerms(server, message.mentions.users.first()) < utils.checkPerms(server, user)){
        if (args.length == 1){
          if (message.mentions.users.first() != null){
            if (!warns[server.id].hasOwnProperty(personID) || warns[server.id][personID].warnings == 0){
              message.channel.sendMessage(person + " has no warnings!");
            }
            else {
              warns[server.id][personID].warnings--;
              var warnsJson = JSON.stringify(warns, null, 2);
              fs.writeFile(__dirname + '/data/warns.json', warnsJson, 'utf8');
              message.channel.sendMessage("✅ Removed a warning from " + person + ". Warnings: `" + warns[server.id][personID].warnings + "/" + warns[server.id].maxwarns + "`");
            }
          }

        }
      }
      else {
        message.channel.sendMessage(user + ", you do not have enough permissions to remove warns from this user.\n(Use `{settings roles` if you do not have moderation roles set up)")
      }
    }

    //Permission Level
    else if (command == "permlvl") {
      if (message.mentions.users.first() != null){
        message.channel.sendMessage(utils.checkPerms(server, message.mentions.users.first()));
      }
      else if (message.content != "") {
        message.channel.sendMessage(utils.checkPerms(server, user));
      }
    }

    //Clear messages
    else if (command == "clear"){
      if (utils.checkPerms(server, user) >= 1){
        if (args.length == 0){
          message.channel.sendMessage("Invalid arguments. Use `" + config.prefix + "help clear` for more information");
        }
        if (args.length == 1){
          try{
            message.channel.fetchMessages({limit: parseInt(args[0])+1})
            .then(messages => {
              messages.map(m => m.delete().catch(console.error) );
            }).catch(console.error);
          }
          catch (err) {
            message.channel.sendMessage("Invalid arguments. Use `" + prefix + "help clear` for more information");
          }
        }
      }
    }

  //End, don't write past here
  }
};
//
// bot.on('guildBanAdd', (server, user) =>{
//   if (svr.hasOwnProperty(server.id) && svr[server.id].hasOwnProperty("logChannel")){
//     var data = new Discord.RichEmbed();
//     data.setColor(parseInt("64dd17", 16)); //Must be decimal color
//     data.setFooter(user.username + "'s Chip");
//     data.setTitle(dat[user.id]["name"].toUpperCase() + "'s stats");
//     data.addField("Health", dat[user.id]["health"]);
//     data.addField("Hunger", dat[user.id]["food"], true);
//     data.addField("Happiness", dat[user.id]["happy"]);
//     data.addField("Mood", dat[user.id]["mood"], true);
//     data.setTimestamp();
//     channel.sendEmbed(data)
//
//   }
// });
