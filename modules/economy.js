const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
var econ = require("./data/economy.json");
const prefix = botInfo.prefix;

var pays = [
  "Ka-ching! ",
  "Ka-chingles! ",
  "Payday! ",
  "Make it rain! ",
  "Dolla Dolla Bill y'all! ",
  "I'm rich! "
]

module.exports = {
  name : "💰 Economy",

  version : 3,

  help : {
    "payday" : "`Usage: " + prefix + "payday`\nGet paid",
    "pay" : "`Usage: " + prefix + "pay <USER> <AMOUNT>`\nPay someone money",
    "balance" : "`Usage: " + prefix + "balance`\nView your balance"
  },

  //The module's commands
  main :  function (message, user, command, args){

    var server = message.guild;

    if (message.mentions.users.first() == null){
      var person = user;
      var personID = user.id;
    }
    if (message.mentions.users.first() != null){
      var person = message.mentions.users.first();
      var personID = person.id;
    }

    if (command == "payday"){
      var d = new Date();
      if (!econ.hasOwnProperty(server.id)) {
        econ[server.id] = {

        };
      }

      if (!econ[server.id].hasOwnProperty(personID)) {
        econ[server.id][personID] = {
          "balance" : 0,
          "totalBalance" : 0,
          "lastCashedIn" : 0
        };
      }

      if (econ[server.id][personID].lastCashedIn + econ.cooldown*1000 <= d.getTime()){
        econ[server.id][personID].balance += econ.payday;
        econ[server.id][personID].totalBalance += econ.payday;
        econ[server.id][personID].lastCashedIn = d.getTime();
        var econJson = JSON.stringify(econ, null, 2);
        fs.writeFile(__dirname + '/data/economy.json', econJson, 'utf8');
        message.channel.sendMessage(pays[Math.floor(Math.random()*pays.length)] + user + " just gained $" + econ.payday);
      }
      else {
        message.channel.sendMessage(user + ", please wait " + Math.round((econ[server.id][personID].lastCashedIn + econ.cooldown*1000 - d.getTime())/1000) + " more seconds until using that command again");
      }

    }

    else if (command == "pay") {
      if (args.length < 2){
        message.channel.sendMessage(user + ", please enter someone to pay and the amount to pay them");
      }
      else if (message.mentions.users.first() == null){
        message.channel.sendMessage(user + ", please enter someone to pay");
      }
      else{
        if (!econ[server.id].hasOwnProperty(personID)) {
          econ[server.id][personID] = {
            "balance" : 0,
            "totalBalance" : 0,
            "lastCashedIn" : 0
          };
        }

        if (!econ[server.id].hasOwnProperty(user.id)) {
          econ[server.id][user.id] = {
            "balance" : 0,
            "totalBalance" : 0,
            "lastCashedIn" : 0
          };
        }

        try{
          if (econ[server.id][user.id].balance >= Math.abs(parseInt(args[1]))){
            econ[server.id][user.id].balance -= Math.floor(Math.abs(parseInt(args[1])));
            econ[server.id][personID].balance += Math.floor(Math.abs(parseInt(args[1])));
            econ[server.id][personID].totalBalance += Math.floor(Math.abs(parseInt(args[1])));
            message.channel.sendMessage(pays[Math.floor(Math.random()*pays.length)] + message.mentions.users.first() + " just gained $" + Math.abs(parseInt(args[1])) + " from " + user);
            var econJson = JSON.stringify(econ, null, 2);
            fs.writeFile(__dirname + '/data/economy.json', econJson, 'utf8');
          }
          else{
            message.channel.sendMessage(user + ", you don't have enough money 🙁");
          }
        }
        catch (err){
          message.channel.sendMessage(user + ", please enter an amount to pay");
        }

      }
    }

    else if (command == "balance") {
      message.channel.sendMessage(person + "'s balance is $" + utils.getMoney(server, person));
    }

  //End, don't write past here
  }
};
