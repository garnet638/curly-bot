const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
var db = require("./data/curlynet.json");
const prefix = botInfo.prefix;

function newProfile(user){
  db[user.id] = {
    "username" : user.username,
    "color" : "1a237e",
    "description" : "",
    "status" : "No status set",
  }
}

function checkIfRegistered(user){
  if (db.hasOwnProperty(user.id)) {
    newProfile(user.id);
  }
}

function sendProfile(user, channel){
  var data = new Discord.RichEmbed();
  data.setColor(parseInt(db[user.id]["color"], 16)); //Must be decimal color
  data.setFooter("CurlyNet v1");
  data.setTitle(db[user.id]["username"]);
  data.addField(db[user.id]["description"]);
  data.addField("Currently: " + db[user.id]["status"]);
  data.setTimestamp();
  channel.sendEmbed(data);
}

module.exports = {
  name : "💬 CurlyNet",

  version : 1,

  help : {
    "profile" : "`Usage: " + prefix + "profile [PERSON]`\nShow your profile",
    "bar" : "`Usage: " + prefix + "bar`\nBar"
  },

  main :  function (message, user, command, args){
    if (command == "profile"){
      sendProfile(user, message.channel);
    }

    else if (command == "bar") {
      message.channel.sendMessage("Bar");
    }
  //End, don't write past here
  }
};
