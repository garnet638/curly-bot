const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
var streamList = require("./data/streams.json")
const prefix = botInfo.prefix;

module.exports = {
  name : "🎶 Audio",

  version : 1,

  //The module's help files
  help : {
    "stream" : "`Usage: " + prefix + "stream [STREAM NAME/URL]`\nStream music online",
    "stop" : "`Usage: " + prefix + "stop`\nStop the current music",
    "addstream" : "`Usage: " + prefix + "addstream <NAME> <URL> [GLOBAL]`\nSave a stream"
  },

  //The module's commands
  main :  function (message, user, command, args){

    server = message.guild;

    if (command == "stop"){
      try{
        message.guild.member(user).voiceChannel.join()
         .then(connection => {
           connection.disconnect();
         })
         .catch(console.error);
      }
      catch (err){}
    }

    if (command == "stream"){
      if (!streamList.hasOwnProperty(server.id)) {
        streamList[server.id] = {

        };
      }
      fs.writeFile(__dirname + '/data/streams.json', JSON.stringify(streamList, null, 2), 'utf8');


      if (args.length == 0){
        msg = "You can use **" + prefix + "stream {URL}** to stream from online\n**Saved streams** ";
        msg += "\n**Global:** ";
        for (var str in streamList["global"]){
          msg += "`" + str + "` ";
        }
        msg += "\n**Server:** ";
        for (var str in streamList[server.id]){
          msg += "`" + str + "` ";
        }
        message.channel.sendMessage(msg);
      }
      else {
        try{
          for (var str in streamList["global"]){
            if (str == args[0].toLowerCase()){
              args[0] = streamList["global"][str];
            }
          }
          for (var str in streamList[server.id]){
            if (str == args[0].toLowerCase()){
              args[0] = streamList[server.id][str];
            }
          }

          const streamOptions = { seek: 0, volume: 0.75 };
          message.guild.member(user).voiceChannel.join()
           .then(connection => {
             const dispatcher = connection.playStream(request(args[0]), streamOptions);
           })
           .catch(console.error);
        }
        catch (err){
          if (err == "TypeError: Cannot read property 'join' of undefined"){
            message.channel.sendMessage(user + " Please join a channel to play music")
          }
        }
      }
    }

    else if (command == "addstream") {
      if (args.length == 2){
        if (!streamList.hasOwnProperty(server.id)) {
          streamList[server.id] = {

          };
        }
        streamList[server.id][args[0]] = args[1];
        fs.writeFile(__dirname + '/data/streams.json', JSON.stringify(streamList, null, 2), 'utf8');
      }
      else if (args.length == 3){
        if (utils.checkPerms(server, user) == 5){
          streamList["global"][args[0]] = args[1];
          fs.writeFile(__dirname + '/data/streams.json', JSON.stringify(streamList, null, 2), 'utf8');
        }
        else{
          message.channel.sendMessage(user + ", sorry, you must be the bot owner to do that");
        }
      }
      else{
        message.channel.sendMessage(utils.errorHelp("addstream", user));
      }
    }
  //End, don't write past here
  }
};
