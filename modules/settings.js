const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
var config = require("../config.json");
var serverJ = require("../server.json");
var warns = require("./data/warns.json");
var botInfo = require("../botinfo.json");
var utils = require("../curlyUtils.js");
const prefix = botInfo.prefix;

module.exports = {

  name : "⚙ Settings",

  version : 1,

  help : {
    "settings" : "`Usage: " + prefix + "settings`\nEdit your server's settings"
  },

  main :  function (message, user, command, args){
    if (command == "settings" || command == "setting"){
      var server = message.guild;


      if (!serverJ.hasOwnProperty(server.id)) {
        serverJ[server.id] = {
          "modRole": "Moderator",
          "adminRole": "Admin",
          "ownerRole": "Owner"
        }
        var serverJson = JSON.stringify(serverJ, null, 2);
        fs.writeFile(__dirname + '/../server.json', serverJson, 'utf8');
      }


      if (utils.checkPerms(message.guild, user) < 4){
        message.channel.sendMessage(user + ", you must be the server owner to change settings.");
      }

      else if (args[0] == "roles" || args[0] == "role"){
        if (args[1] == "mod"){
          serverJ[server.id].modRole = args.slice(2).join(" ");
          var serverJson = JSON.stringify(serverJ, null, 2);
          fs.writeFile(__dirname + '/../server.json', serverJson, 'utf8');
          message.channel.sendMessage("Set moderator role to: `" + args.slice(2).join(" ") + "`");
        }
        else if (args[1] == "admin"){
          serverJ[server.id].adminRole = args.slice(2).join(" ");
          var serverJson = JSON.stringify(serverJ, null, 2);
          fs.writeFile(__dirname + '/../server.json', serverJson, 'utf8');
          message.channel.sendMessage("Set admin role to: `" + args.slice(2).join(" ") + "`");
        }
        else if (args[1] == "owner"){
          serverJ[server.id].ownerRole = args.slice(2).join(" ");
          var serverJson = JSON.stringify(serverJ, null, 2);
          fs.writeFile(__dirname + '/../server.json', serverJson, 'utf8')
          message.channel.sendMessage("Set owner role to: `" + args.slice(2).join(" ") + "`");;
        }
        else{
          message.channel.sendMessage("**SETTINGS**\n`mod` Change the moderator role (Level 1)\n`admin` Change the admin role (Level 2)\n`owner` Change the server owner role (Level 3)");
        }
      }

      else if (args[0] == "maxwarns" || args[0] == "warns"){
        if (args.length == 2){
          try{
            if (!warns.hasOwnProperty(server.id)) {
              warns[server.id] = {
                "maxwarns" : 3
              };
            }
            warns[server.id].maxwarns = parseInt(args[1]);
            var warnsJson = JSON.stringify(warns, null, 2);
            fs.writeFile(__dirname + '/../warns.json', warnsJson, 'utf8');
            message.channel.sendMessage("Set max warns to: `" + args[1] + "`");
          }
          catch (err) {
            message.channel.sendMessage("Max Warns must be a number!");
          }
        }
      }

      else if (args[0] == "nsfw"){
        
      }

      else {
        message.channel.sendMessage("**SETTINGS**\n`roles` Change moderation roles\n`maxwarns` Change how many possible warnings someone can have");
      }

    }
  //End, don't write past here
  }
};
