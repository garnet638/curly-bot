# NOTICE
Please note, as of 7/31/2017, all development on CurlyBot has ceased.

Curly was my first Discord bot, and I have learned a lot about the Discord API and Node.JS since I began work on CurlyBot.

I am ceasing development to work on a new Discord bot, along with some personal projects I hope to release soon.

I will no longer be supporting CurlyBot, and all the code I have just pushed may or may not work, so use at your discretion.








![CurlyBot](http://i.imgur.com/muYUaty.png)
# CurlyBot
## A multi-function highly customizable bot written in Discord.JS
CurlyBot is a one-of-a-kind bot. Written in Discord.JS, she is a completely modular bot, allowing you to change your code on the fly. No need to restart your bot for every little change, if you want to load in your new casino, just type `{load casino`, or if you want to push your changes for the fun and games just type `{reload fun`. You can even unload plugins too!


CurlyBot helps with moderation, fun and utilities by default, but can be expanded on further and further to your heart's desire.




**Current Version:** 6


[Trello](https://trello.com/b/35Lsl2Uq/curlybot-development)


### Requirements
[Node.JS](https://nodejs.org/en/download/)


[Discord.JS](https://discord.js.org/#/)


[Git](https://git-scm.com/downloads) (Obviously)


### Install
1. Create a bot account and get its token and Client ID
[Instructions here](http://i.imgur.com/i9ZjZUC.gifv)
[(Discord Developers)](https://discordapp.com/developers/applications/me)
2. Create a new folder for the bot
3. Install Discord.JS into that folder using `npm install discord.js node-opus --save`
4. Right click then "Git Bash" in the folder
5. Type `git init`
6. Type `git clone https://gitlab.com/garnet638/curly-bot.git`
7. Edit `botInfo.json` with your bot's information
8. Change `avatar.png` to your bot's avatar
9. Once you're all finished, type `node curly.js` in a command prompt in the root folder
10. Edit the below invite link with your bot's Client ID

```
https://discordapp.com/oauth2/authorize?permissions=1610083454&scope=bot&client_id=XXXXXXXXXXXXXX
```
11. Invite the bot to a server
12. Profit!

### Support the creator
CurlyBot surprisingly is quite hard and time-consuming to develop. Please consider supporting the creator


**Bitcoin:** 1BXkcKzoBrPvKK3tqbWs43w7zggeGTVaMz
