const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var config = require("./config.json");
var serverJ = require("./server.json");
var botInfo = require("./botinfo.json");
var econ = require("./modules/data/economy.json");
///
/// CURLY UTILS
///
// Here are a bunch of utilities for Curly Brace to utilize.
// These include permission checking and more

module.exports = {
  checkPerms : function (server, user){

    var ownerRole = 0;
    var adminRole = 0;
    var modRole = 0;

    if (!serverJ.hasOwnProperty(server.id)) {
      serverJ[server.id] = {
        "modRole" : "Mod",
        "adminRole" : "Admin",
        "ownerRole" : "Owner"
      };
      var serverJson = JSON.stringify(serverJ, null, 2);
      fs.writeFile('server.json',  JSON.stringify(serverJ, null, 2), 'utf8');
    }
    if (!serverJ[server.id].hasOwnProperty("modRole")) {
      serverJ[server.id].modRole = "Mod";
      var serverJson = JSON.stringify(serverJ, null, 2);
      fs.writeFile('server.json',  JSON.stringify(serverJ, null, 2), 'utf8');
    }
    if (!serverJ[server.id].hasOwnProperty("adminRole")) {
      serverJ[server.id].adminRole = "Admin";
      var serverJson = JSON.stringify(serverJ, null, 2);
      fs.writeFile('server.json',  JSON.stringify(serverJ, null, 2), 'utf8');
    }
    if (!serverJ[server.id].hasOwnProperty("ownerRole")) {
      serverJ[server.id].ownerRole = "Owner";
      var serverJson = JSON.stringify(serverJ, null, 2);
      fs.writeFile('server.json',  JSON.stringify(serverJ, null, 2), 'utf8');
    }


    server.roles.forEach(function(role) {
      if (role.name == serverJ[server.id].ownerRole){
        ownerRole = role.id;
      }
      else if (role.name == serverJ[server.id].adminRole){
        adminRole = role.id;
      }
      else if (role.name == serverJ[server.id].modRole){
        modRole = role.id;
      }
    });

    var gUser = server.member(user);
    if (botInfo.owner == user.id){
      return 5;
    }
    else if (server.ownerID == user.id){
      return 4;
    }
    else{
      if (gUser.roles.get(ownerRole)){
        return 3;
      }
      else{
        if (gUser.roles.get(adminRole)){
          return 2;
        }
        else{
          if (gUser.roles.get(modRole)){
            return 1;
          }
          else{
            return 0;
          }
        }
      }
    }
  },

  checkIfLoaded : function (neededMod){
    if (config.modules.includes(neededMod)){
      return true;
    }
    else{
      return false;
    }
  },

  isOwner : function (user){
    if (botInfo.owner == user.id){
      return true;
    }
    else{
      return false;
    }
  },

  addMoney : function (server, user, money){
    if (!econ.hasOwnProperty(server.id)) {
      econ[server.id] = {

      };
    }

    if (!econ[server.id].hasOwnProperty(user.id)) {
      econ[server.id][user.id] = {
        "balance" : 0,
        "totalBalance" : 0,
        "lastCashedIn" : 0
      };
    }

    econ[server.id][user.id].balance += money;
    econ[server.id][user.id].totalBalance += money;
    var econJson = JSON.stringify(econ, null, 2);
    fs.writeFile(__dirname + '/modules/data/economy.json', econJson, 'utf8');

  },

  getMoney : function (server, user){
    if (!econ.hasOwnProperty(server.id)) {
      econ[server.id] = {

      };
    }

    if (!econ[server.id].hasOwnProperty(user.id)) {
      econ[server.id][user.id] = {
        "balance" : 0,
        "totalBalance" : 0,
        "lastCashedIn" : 0
      };
    }
    var econJson = JSON.stringify(econ, null, 2);
    fs.writeFile(__dirname + '/modules/data/economy.json', econJson, 'utf8');

    return(econ[server.id][user.id].balance);
  },

  makeInt: function(number, args, message, defaultInt, errorMsg){
    try{
      return parseInt(args[number]);
    }
    catch (err) {
      console.log(err)
      if (defaultInt == null){
        defaultInt = 0;
      }
      if (errorMsg != null){
        message.channel.sendMessage(errorMsg);
      }
      return defaultInt;
    }
  },

  shuffle: function(a){
    var j, x, i;
    var b = a.slice(0)
    for (i = b.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = b[i - 1];
        b[i - 1] = b[j];
        b[j] = x;
    }
    return b;
  },

  errorHelp : function(command, user){
    return (user + " Invalid arguments. For more information, use **" + botInfo.prefix + "help " + command + "**");
  }
}
