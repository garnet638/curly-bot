const Discord = require('discord.js');
const bot = new Discord.Client();
var fs = require("fs");
var request = require("request");
const ytdl = require('ytdl-core');
var config = require("./config.json");
var botInfo = require("./botinfo.json");
var botCommand = botInfo.botCommand;
const botToken = botInfo.token
var utils = require("./curlyUtils.js");

module.exports = {
  ebot : bot
}


bot.on('ready', () => {
  bot.user.setAvatar("./avatar.png");
  bot.user.setGame(botInfo.game);

  config.modules.forEach(function(mod) {
    modules[mod] = require("./modules/" + mod + ".js");
  });

  try{
    request('https://gitlab.com/garnet638/curly-bot/raw/master/config.json', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        pog = JSON.parse(body)
        if (pog.curlyVersion > config.curlyVersion){
          console.log("[NOTICE] There is an update for Curly available");
          console.log("[NOTICE] Get it here https://gitlab.com/garnet638/curly-bot");
          console.log("Current version: " + config.curlyVersion + " || Latest version: " + pog.curlyVersion);
        }
        else{
          console.log("Curly is up to date!");
        }
      }
    })
  }
  catch (err){
    console.log("Rippity dippity, something happened Curly couldn't check for updates");
  }


  console.log('Curly loaded!');
});

const prefix = botInfo.prefix;

var modules = {

};

bot.on('message', message => {
  if (message.author.bot) return;
  if (!message.content.startsWith(prefix)) return;

  var user = message.author;

  if (message.guild == null){
    console.log(user.username + " tried using a command in PMs");
  }
  else{
    var curly = bot.user;
    var gCurly = message.guild.member(curly);

    let command = message.content.split(" ")[0];
    command = command.slice(prefix.length).toLowerCase();

    let args = message.content.split(" ").slice(1);

    var date = new Date();
    var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    console.log(timestamp + " (" + utils.checkPerms(message.guild, user) + ") (" + message.guild.name + ") " + user.username + ": " + message.content);

    try{
      for (var mod in modules){
        modules[mod].main(message, user, command, args);
      }
    }
    catch (err) {
      var data = new Discord.RichEmbed();
      data.setColor(parseInt("FF0000", 16)); //Must be decimal color
      data.setFooter("Jinkies!");
      data.setTitle("Uh-oh! An error occured!");
      data.setDescription(err);
      data.setTimestamp();
      message.channel.sendEmbed(data);
      // message.channel.sendCode("js", "Oh-oh! An error occured!\n" + err);
      console.log(err)
    }

    if (command == "help"){
      if (args.length == 0) {
        var commands = "";
        commands += user + ", here ya go!";
        commands += "\n🤖 **Prefix:** `" + prefix + "`";
        for (var mod in modules){
          commands += "\n**" + modules[mod].name + "** "
          for (var cmd in modules[mod].help){
            commands += "`" + cmd + "` ";
          }
        }
        message.channel.sendMessage(commands);
      }
      else if (args.length == 1){
        help = "Sorry, that command could not be found.";
        for (var mod in modules){
          for (var cmd in modules[mod].help){
            if (args[0].toLowerCase() == cmd){
              help = "**" + cmd.toUpperCase() + ":**\n" + modules[mod]["help"][cmd];
            }
          }
        }
        message.channel.sendMessage(help);
      }
      else {
        message.channel.sendMessage("`Usage: " + prefix + "help [COMMAND]`\nHelp files");
      }
    }

    else if (botInfo.owner == user.id){
     try{
       if (command == "load") {
          if (modules.hasOwnProperty(args[0])){
            con
          }
          modules[args[0]] = require("./modules/" + args[0] + ".js");
          config.modules.push(args[0]);
          fs.writeFile('config.json',  JSON.stringify(config, null, 2), 'utf8');
          message.channel.sendMessage("Successfuly loaded `" + args[0] + "`!");
       }

       else if (command == "reload") {
         delete require.cache[require.resolve("./modules/" + args[0] + ".js")];
         modules[args[0]] = require("./modules/" + args[0] + ".js");
         message.channel.sendMessage("Successfuly reloaded `" + args[0] + "`!");
       }

       else if (command == "unload") {
         delete require.cache[require.resolve("./modules/" + args[0] + ".js")];
         var i = config.modules.indexOf(args[0]);
         if(i != -1) {
           config.modules.splice(i, 1);
         }
         fs.writeFile('config.json',  JSON.stringify(config, null, 2), 'utf8');
         message.channel.sendMessage("Successfuly unloaded `" + args[0] + "`!");
       }

       else if (command == "listmod") {
         message.channel.sendMessage("**Active modules: **`" + config.modules.join("`, `") + "`");
       }
     }
     catch (err) {
        if (err.message.indexOf("Cannot find module") > -1){
          message.channel.sendCode("js", err);
        }
        else{
          message.channel.sendCode("js", "Oh-oh! An error occured loading the module!\n" + err);
          console.log(err);
        }
     }
     if (command == "name"){
       if (args.length > 0){
         bot.user.setUsername(args.join(" "));
       }
     }
   }
 }

  //The end
});

bot.login(botToken);
